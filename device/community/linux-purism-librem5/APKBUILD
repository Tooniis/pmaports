# Reference: <https://postmarketos.org/vendorkernel>
# Maintainer:  Clayton Craft <clayton@craftyguy.net>
pkgname=linux-purism-librem5
pkgver=5.11.4
pkgrel=3
_purismrel=1
# <kernel ver>.<purism kernel release>
_purismver=${pkgver}pureos$_purismrel
pkgdesc="Purism Librem 5 phone kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="purism-librem5"
url="https://source.puri.sm/Librem5/linux-next"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="
	bison
	devicepkg-dev
	findutils
	flex
	installkernel
	openssl-dev
	perl
	rsync
	xz
	"
subpackages="$pkgname-dev"

# Source
_repository="linux-next"
# kconfig generated with: ARCH=arm64 make defconfig KBUILD_DEFCONFIG=librem5_defconfig
_config="config-$_flavor.$arch"
source="
	$pkgname-$_purismver.tar.gz::https://source.puri.sm/Librem5/$_repository/-/archive/pureos/$_purismver/$_repository-pureos-$_purismver.tar.gz
	8f11380ec32912370b8ae9134a0387a6f18862f7.patch
	0001-Revert-arm64-dts-librem5-Drop-separte-DP-device-tree.patch
	0002-bq25890_charger-enter-ship-mode-on-power-off.patch
	0003-Revert-redpine-Clean-up-loop-in-the-interrupt-handler.patch
	$_config
"
builddir="$srcdir/$_repository-pureos-$_purismver"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" \
		LOCALVERSION=".$_purismrel"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"

}

dev() {
	provides="linux-headers"
	replaces="linux-headers"

	cd $builddir

	# https://github.com/torvalds/linux/blob/master/Documentation/kbuild/headers_install.rst
	make -j1 headers_install \
		ARCH="$_carch" \
		INSTALL_HDR_PATH="$subpkgdir"/usr
}
sha512sums="d2e0f49403413f581aee6e47e021cb04fc003fbf821349c188a0c6a57561ba702c327badf9b181c6f8fdc6952ebc7f061482d6508ae48dc336842eb94c5ed558  linux-purism-librem5-5.11.4pureos1.tar.gz
9870bff4b187188b519b23264c2634ee4232011fed6d2f66a7b4971db354ac3dffa0e1552bd0dc953c66ec622e18ce8899fdbcfba94f60867fc5004d6da96753  8f11380ec32912370b8ae9134a0387a6f18862f7.patch
5baae99010bde62e253fdd56f21ba096c217ba2ab9c367c80b34bc0f445a79a8fb8b5d14682f71ad6061d73c81fc16a608f4be037d792978dbbaf74267844260  0001-Revert-arm64-dts-librem5-Drop-separte-DP-device-tree.patch
1a12f74895b0fc710792e3881f23754a8eb92d25b11a2751db007a1b08f72729043d1e824096c97dc795b8e33300274887b428997ddaacf4b61f52ef3bd78ce5  0002-bq25890_charger-enter-ship-mode-on-power-off.patch
00286a7ea3d3167150eca1025f271bb76e05d8a47c481879b1322ec9e88250c365b1b0ddcc8140ccc9b8f6138ffec843f184645c0c7d4ff11f75c988f5c4945c  0003-Revert-redpine-Clean-up-loop-in-the-interrupt-handler.patch
07ce9dfb837e8a1ae10bc0edbbca107a3b12a2e5e2935fa9a1aa24c8f31e3c564685f2cb4a7b231960bdc666bcead1a946645ec7615b985d5c3d0cf6babaf098  config-purism-librem5.aarch64"
