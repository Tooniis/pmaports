# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
pkgname=postmarketos-mkinitfs
pkgver=0.22
pkgrel=1
pkgdesc="Tool to generate initramfs images for postmarketOS"
url="https://postmarketos.org"
depends="
	busybox-extras
	bzip2
	charging-sdl
	multipath-tools
	cryptsetup
	device-mapper
	e2fsprogs
	e2fsprogs-extra
	kmod
	lddtree
	lz4
	osk-sdl
	parted
	triggerhappy
	xz
	"
replaces="mkinitfs"
triggers="$pkgname.trigger=/etc/postmarketos-mkinitfs/hooks:/usr/share/kernel/*:/usr/share/postmarketos-mkinitfs-triggers"
source="00-default.modules
	init.sh.in
	init_functions.sh
	mkinitfs.sh
	mkinitfs_functions.sh
	mkinitfs_test.sh
	"
arch="noarch"
license="GPL-2.0-or-later"
provides="mkinitfs=0.0.1"

package() {
	for file in init.sh.in init_functions.sh mkinitfs_functions.sh; do
		install -Dm644 "$srcdir/$file" \
			"$pkgdir/usr/share/postmarketos-mkinitfs/$file"
	done

	install -Dm644 "$srcdir/00-default.modules" \
		"$pkgdir/etc/postmarketos-mkinitfs/modules/00-default.modules"

	install -Dm755 "$srcdir/mkinitfs.sh" \
		"$pkgdir/sbin/mkinitfs"

	mkdir -p "$pkgdir/etc/postmarketos-mkinitfs/hooks/"
}

check() {
	/bin/busybox sh ./mkinitfs_test.sh
}

sha512sums="5037cb7285bb7c0c40ca9e6df332d882ef9a8b379756c785f921e062dab1b7e7f3139d00897f69323a916d709ced4297fea8cbd3a13ebae575b873ec9e2cbfae  00-default.modules
bafd06286594102b8b3b126c3ae0a77a97f004ab804f03426154310c5107a1acaf3636bdba92626333adfe4fb0df32ff42c6d8d9e7adf35f6da620c6e14407a1  init.sh.in
9a18ccd14ab5a9e2decc3df35c9b489cbb94b57dc180f0919d0cdf003de5f0f4730beb96d5c3d19b13aebc6d6fe046aa9e81846b86a66f794ca1cb59b91eabfe  init_functions.sh
dfc01ee0547ea88b7aa45a005e842b636e9e19bbf1705f3dad53a66d57af7c5c513c092b5469a06d9b00322e56a4d25f1b47e4c5324aafa99f5291679968d1f1  mkinitfs.sh
fa5a6d99f05988324b8dbf35f02eee276448c04775dad85ecdd21ed0f8009472143038334967a629a021633031f6e376cc2307423d94b00e99e9a5789337aaa8  mkinitfs_functions.sh
c7a3c33daeb12b33ac72207191941c4d634f15c22958273b52af381a70ebaba1d3a9299483f0c447d9e66c560151fe7b9588bb4bbef2c8914f83185984ee4622  mkinitfs_test.sh"
